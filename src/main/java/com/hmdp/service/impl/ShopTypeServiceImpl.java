package com.hmdp.service.impl;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.hmdp.dto.Result;
import com.hmdp.entity.Shop;
import com.hmdp.entity.ShopType;
import com.hmdp.mapper.ShopTypeMapper;
import com.hmdp.service.IShopTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

import static com.hmdp.utils.RedisConstants.CACHE_SHOP_TYPE_LIST_KEY;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 虎哥
 * @since 2021-12-22
 */
@Service
public class ShopTypeServiceImpl extends ServiceImpl<ShopTypeMapper, ShopType> implements IShopTypeService {

    @Resource
    private StringRedisTemplate stringRedisTemplate;
    @Override
    public Result queryTypeList() {
        // 1.从redis中查询商铺类型缓存
        String key = CACHE_SHOP_TYPE_LIST_KEY;
        String shopTypeListJson = stringRedisTemplate.opsForValue().get(key);
        // 2.判断是否存在
        if (StrUtil.isNotBlank(shopTypeListJson)) {
            // 3.如果存在，直接返回
            List<ShopType> typelist = JSONUtil.toList(shopTypeListJson, ShopType.class);
            return Result.ok(typelist);
        }
        // 4.如果不存在，查询数据库
        List<ShopType> typeList = query().orderByAsc("sort").list();
        // 5.判断是否存在
        if (typeList == null) {
            // 6.如果不存在，直接返回错误
            return Result.fail("店铺类型列表不存在");
        }
        // 7.如果存在，写入到redis
        stringRedisTemplate.opsForValue().set(key, JSONUtil.toJsonStr(typeList));
        // 8. 返回
        return Result.ok(typeList);
    }
}
